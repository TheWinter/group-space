const express = require('express');
const expressLayouts = require('express-ejs-layouts');
const mongoose = require('mongoose');
const flash = require('connect-flash');
const session = require('express-session');
var FileStore = require('session-file-store')(session);
const passport = require('passport');
const multer = require('multer');
const path = require('path');
const fs = require('fs');
const moment = require('moment');
const { ensureAuthenticated } = require('./config/auth');
const sanitize = require("sanitize-filename");
const secrets = require("./.secrets/secrets.js")

const Notification = require('./models/Notification');

// Multer Setup and Routes
// TODO
// storage engine
const storage = multer.diskStorage({
    // where to upload files
    destination: './public/uploads/',
    filename: function (req, file, callback) {
        callback(null, sanitize(file.originalname) + '-' + Date.now()); //  + path.extname(file.originalname) to show .filetype
    }
});

// Init upload
// uploading single files at a time
var upload = multer({ storage: storage }).array('u', 5);

// joining path of directory
const directoryPath = path.join(__dirname, '/public/uploads');

// initializing app variable with express
const app = express();

// Passport config
require('./config/passport')(passport)

// DB Config
// const db = require('./config/keys').MongoURI;
const db = require('./config/db').uri;

mongoose.set('useFindAndModify', false);
// Connect to Mongo
mongoose.connect(db, { useNewUrlParser: true })
    .then(() => console.log("MongoDB connected"))
    .catch(err => console.log(err))

// EJS
app.use(expressLayouts);
app.set('view engine', 'ejs');

// Public Folder
app.use(express.static('./public'))

// BodyParser
// allows us to get data from our form with req.body
app.use(express.urlencoded({ extended: false }))


// Passport middleware
// this needs to be below express session
// Express Session
app.use(session({
    // store: new FileStore({}), // Comment out to edit css. stores the session in file rather than memory
    secret: secrets['session'],
    resave: true,
    saveUninitialized: true
}))
app.use(passport.initialize());
app.use(passport.session());

// Connect flash
app.use(flash());

// Global Vars
// custom middleware
app.use((req, res, next) => {
    res.locals.success_msg = req.flash("success_msg");
    res.locals.error_msg = req.flash("error_msg");
    res.locals.error = req.flash("error");
    next()
})


// Routes
app.use('/', require('./routes/index'));
// /users/login
app.use('/users', require('./routes/users'));
app.use('/events', require('./routes/events'));

// file upload route
app.post('/upload', ensureAuthenticated, function (req, res) {
    Notification.find({})
        .then(notifications => {
            const files = fs.readdirSync(directoryPath)
            upload(req, res, (err) => {
                console.log(req.file);
                if (err) {
                    res.render('dashboard', {
                        name: req.user.name,
                        admin: req.user.isAdmin,
                        msg: err,
                        files: files.sort(),
                        notifications
                    })
                } else {
                    if (req.file == undefined) {
                        res.render('dashboard', {
                            name: req.user.name,
                            admin: req.user.isAdmin,
                            msg: 'Error: no file selected',
                            files: files.sort(),
                            notifications,
                            moment: moment
                        })
                    } else {
                        // getting all filenames as array from uploads dir
                        res.redirect('/dashboard')
                    }
                }
            });
        })

});

// process.env.port in case we deploy
const PORT = process.env.PORT || 5000;

app.listen(PORT, console.log(`Server started on port ${PORT}`))