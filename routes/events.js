const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const { ensureAuthenticated } = require('../config/auth')
const whitelist = require("../.secrets/whitelist").whitelist;
const admins = require("../.secrets/whitelist").admins;
const utils = require("../config/show-hide.js");
const User = require('../models/User').User;
const Event = require('../models/Event');
const moment = require('moment');

function removeA(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax = arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}

router.get('/events', ensureAuthenticated, (req, res) => {
    Event.find()
        .populate({ path: 'participants', model: 'User' })
        .populate({ path: 'cancellations', model: 'User' })
        .populate({ path: 'declines', model: 'User' })
        .sort('nastup')
        .then(function (events) {
            return events
        })
        .then(events => {
            res.render("events", {
                name: req.user.name,
                email: req.user.email,
                admin: req.user.isAdmin,
                userId: req.user._id,
                events: events,
                utils: utils
            });
        })
        .catch(err => console.log(err));
})

router.post('/create', ensureAuthenticated, (req, res) => {
    Event.find({}, function (err, events) {

    })
        .then(events => {

            const { title, description, nastup } = req.body;

            // Validation
            let evntErrs = [];

            if (!title || !description || !nastup) {
                evntErrs.push({ msg: 'Please fill in all fields' });
            }

            if (evntErrs.length > 0) {
                // re-render the registration form, and display error messages and the values (pass them to EJS)
                res.render('events', {
                    evntErrs,
                    name: req.user.name,
                    email: req.user.email,
                    admin: req.user.isAdmin,
                    userId: req.user._id,
                    title,
                    description,
                    nastup,
                    events,
                    utils: utils
                })
            } else {

                // Check the format of date passed
                // if its incorrect - DD/MM/YYYY, then we need to change it
                var nastupDate;
                // moment forgiving mode
                var checkIfValid = moment(nastup, 'MM/DD/YYYY hh:mm A').format('MM/DD/YYYY HH:mm')

                substring = "nvalid";
                // if date invalid then execute this logic
                if (checkIfValid.includes(substring)) {
                    var nastupDashed;
                    if (nastup.match(/\//) != null) {
                        nastupDashed = nastup.replace(/\//g, '-');
                    } else {
                        nastupDashed = nastup
                    }

                    var sub = nastupDashed.substring(0, 10);
                    // get AM or PM
                    var ampm = nastupDashed.slice(-2)

                    var n = nastupDashed.indexOf(":")
                    // 2 up 2 down for time
                    var up = n + 3;
                    var down = n - 2;

                    // time digits
                    var time = nastupDashed.slice(down, up);


                    var splits = sub.split("-");
                    var dateStr = splits[1] + "-" + splits[0] + "-" + splits[2] + " " + time + " " + ampm;

                    nastupDate = new Date(dateStr);
                } else {
                    nastupDate = moment(checkIfValid);
                }


                // var formattedTime = moment(nastup).format('MM/DD/YYYY HH:mm');
                // console.log("nastup", nastup);
                // var nastupDate = new Date(formattedTime);

                // create new event
                const newEvent = new Event({
                    _id: new mongoose.Types.ObjectId(),
                    title,
                    description,
                    nastup: nastupDate,
                    users: []
                });

                newEvent.save()
                    .then(event => {
                        // this creates a flash message, but doesnt display it.
                        // to display the msg, you need to use ejs
                        // req.flash('success_msg', 'You are now registered and can log in')
                        res.redirect('/events/events')
                    })
                    .catch(err => console.log(err))
            }

        });

})

router.post('/user-confirm', ensureAuthenticated, (req, res) => {
    const { theid, email } = req.body;
    User.findOne({ email: email }, function (err, user) {
        // console.log(user);
        Event.findOne({ _id: theid }, (err, event) => {

            // check if the user has either cancelled or declined
            var declineFound = event.declines.filter(decline => {
                // console.log("comparison", decline._id, user._id)
                return decline._id.toString() === user._id.toString();
            });

            var cancellationFound = event.cancellations.filter(cancellation => {
                // console.log("comparison", participant._id, user._id)
                return cancellation._id.toString() === user._id.toString();
            });

            // if they have declined, move them from there to participants
            if (declineFound.length > 0) {
                removeA(event.declines, declineFound[0])
                event.participants.push(user._id)
            } else if (cancellationFound.length > 0) {
                removeA(event.cancellations, cancellationFound[0])
                event.participants.push(user._id)
            } else {
                event.participants.push(user._id)
            }

            event.save()
                .then(() => {
                    res.redirect('/events/events');
                })
                .catch(err => console.log(err))
        })
    })
})


router.post('/user-decline', ensureAuthenticated, (req, res) => {
    const { theid, email } = req.body;
    User.findOne({ email: email }, function (err, user) {
        // console.log(user);
        Event.findOne({ _id: theid }, (err, event) => {
            // check if the user has indicated they areparticipating
            var userFound = event.participants.filter(participant => {
                // console.log("comparison", participant._id, user._id)
                return participant._id.toString() === user._id.toString();
            });
            // if user has indicated participating,  a) remove them from particpants b) put them in cancellations
            // otherwise put them in declines array
            if (userFound.length > 0) {
                removeA(event.participants, userFound[0])
                event.cancellations.push(user._id)
            } else {
                event.declines.push(user._id)
            }

            event.save()
                .then(() => {
                    res.redirect('/events/events');
                })
                .catch(err => console.log(err))
        })
    })
})


// Delete an Event
router.post('/:id', ensureAuthenticated, (req, res) => {
    const id = req.params.id;

    Event.findOneAndRemove({ '_id': id }, function (err, event) {
        if (err) { throw err; }
        res.redirect('/events/events');
    });
})

// Edit an Event
router.get('/edit/:id', ensureAuthenticated, (req, res) => {
    const id = req.params.id;
    Event.findOne({ _id: id }, (err, event) => {
        if (err) { throw err; }
        res.render('edit-event', {
            admin: req.user.isAdmin,
            title: event.title,
            description: event.description,
            nastup: event.nastup,
            id: event._id
        });
    })
})

// Update Event
router.post('/update/:id', ensureAuthenticated, (req, res) => {
    // console.log(req.body, req.params);
    const id = req.params.id;
    const { title, description, nastup } = req.body;
    Event.findOne({ _id: id }, (err, event) => {

        // Validation
        let evntErrs = [];

        if (!title || !description || !nastup) {
            evntErrs.push({ msg: 'Please fill in all fields' });
        }

        if (evntErrs.length > 0) {
            // re-render the registration form, and display error messages and the values (pass them to EJS)
            res.redirect(`/events/edit/${id}`)
        } else {
            var nastupDate;
            var checkIfValid = moment(nastup, 'MM/DD/YYYY hh:mm A').format('MM/DD/YYYY HH:mm')
            substring = "nvalid";
            // if date invalid then execute this logic
            if (checkIfValid.includes(substring)) {
                console.log("date bad format", nastup)
                var nastupDashed;
                if (nastup.match(/\//) != null) {
                    nastupDashed = nastup.replace(/\//g, '-');
                } else {
                    nastupDashed = nastup
                }

                var sub = nastupDashed.substring(0, 10);
                // get AM or PM
                var ampm = nastupDashed.slice(-2)

                var n = nastupDashed.indexOf(":")
                // 2 up 2 down for time
                var up = n + 3;
                var down = n - 2;

                // time digits
                var time = nastupDashed.slice(down, up);

                var splits = sub.split("-");
                var dateStr = splits[1] + "-" + splits[0] + "-" + splits[2] + " " + time + " " + ampm;

                // console.log("datestr: ", dateStr);
                nastupDate = new Date(dateStr).toISOString();
            } else {
                nastupDate = new Date(checkIfValid).toISOString();
            }

            event.title = title;
            event.description = description;
            event.nastup = nastupDate;

            event.save()
                .then(() => {
                    res.redirect('/events/events');
                })
                // .catch(err => console.log(err))
        }
    })
})

module.exports = router;