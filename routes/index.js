const express = require('express');
const router = express.Router();
const { ensureAuthenticated } = require('../config/auth')
const fs = require('fs');
const path = require('path');
const sanitize = require("sanitize-filename");
const mongoose = require('mongoose');
const moment = require('moment');
const momenttz = require('moment-timezone');
const Notification = require('../models/Notification');

// joining path of directory
const directoryPath = path.join(__dirname, '../public/uploads');

router.get('/', (req, res) => {
    res.render("welcome");
})

router.get('/performances', ensureAuthenticated, (req, res) => {
    res.render("performance-details");
})

// Gets the files in uploads folder
router.get('/dashboard', ensureAuthenticated, (req, res) => {
    Notification.find({})
        .sort('-date')
        .then(notifications => {
            // getting all filenames as array from uploads dir
            var files = fs.readdirSync(directoryPath)
            // when we're logged in, we have access to req.user
            res.render("dashboard", {
                name: req.user.name, // better pass just name than entire user
                admin: req.user.isAdmin,
                files: files.sort(),
                notifications: notifications,
                moment: moment,
                momenttz: momenttz
            });
        })
})

router.post('/upload/:filename', ensureAuthenticated, (req, res) => {
    const filename = sanitize(req.params.filename);
    fs.unlink(`${directoryPath}/${filename}`, (err) => {
        if (err) throw err;
        res.redirect('/dashboard')
    });
});

router.post('/new/notification', ensureAuthenticated, (req, res) => {

    const { content } = req.body;

    Notification.find({})
        .then(notifications => {
            var files = fs.readdirSync(directoryPath);
            // Validation
            let notifErrors = [];

            if (!content) {
                notifErrors.push({ msg: 'Please fill in the notification' });
            }

            if (notifErrors.length > 0) {
                // re-render the registration form, and display error messages and the values (pass them to EJS)
                res.render('dashboard', {
                    notifErrors,
                    name: req.user.name,
                    admin: req.user.isAdmin,
                    files: files.sort(),
                    content,
                    notifications,
                    moment: moment,
                    momenttz: momenttz
                })
            } else {
                const newNotification = new Notification({
                    _id: new mongoose.Types.ObjectId(),
                    content
                });

                newNotification.save()
                    .then(event => {
                        // this creates a flash message, but doesnt display it.
                        // to display the msg, you need to use ejs
                        // req.flash('success_msg', 'You are now registered and can log in')
                        res.redirect('/dashboard')
                    })
                    .catch(err => console.log(err))
            }
        })
});

// Edit a Notification
router.get('/edit/notification/:id', ensureAuthenticated, (req, res) => {
    const id = req.params.id;
    Notification.findOne({ _id: id }, (err, notification) => {
        if (err) { throw err; }
        res.render('edit-notification', {
            admin: req.user.isAdmin,
            content: notification.content,
            id: notification._id,
            moment: moment,
            momenttz: momenttz
        });
    })
})

// Update N
router.post('/update/notification/:id', ensureAuthenticated, (req, res) => {
    const id = req.params.id;
    const { content } = req.body;
    Notification.findOne({ _id: id }, (err, notification) => {

        // Validation
        let evntErrs = [];

        if (!content) {
            evntErrs.push({ msg: 'Please fill in all fields' });
        }

        if (evntErrs.length > 0) {
            // re-render the registration form, and display error messages and the values (pass them to EJS)
            res.redirect(`/edit/notification/${id}`)
        } else {
            notification.date = new Date().toISOString();
            notification.content = content;
            notification.save()
                .then(() => {
                    res.redirect('/dashboard');
                })
        }
    })
})

// Remove a notification
router.post('/remove/notification/:id', ensureAuthenticated, (req, res) => {

    const id = req.params.id;

    Notification.findOneAndRemove({ '_id': id }, function (err, notification) {
        if (err) { throw err; }
        res.redirect('/dashboard');
    });
});

module.exports = router;