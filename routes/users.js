const express = require('express');
const router = express.Router();
// password encryption
const bcrypt = require('bcryptjs');
const passport = require('passport');
const mongoose = require('mongoose');
const async = require("async");
const nodemailer = require("nodemailer");
const crypto = require("crypto");

const whitelist = require("../.secrets/whitelist").whitelist;
const admins = require("../.secrets/whitelist").admins;
const uploader = require("../.secrets/whitelist").uploader;
const database = require("../.secrets/config");

const User = require('../models/User').User;

router.get('/login', (req, res) => {
    res.render("login");
})

router.get('/register', (req, res) => {
    res.render("register");
})

router.get('/abc/sent', (req, res) => {
    res.render("sent");
})

// Register Handle
router.post('/register', (req, res) => {
    const { name, lname, email, code, password, password2 } = req.body;

    // Validation
    let errors = [];

    // Check required fields
    if (!name || !lname || !email || !code || !password || !password2) {
        errors.push({ msg: 'Please fill in all fields' });
    }

    // Check passwords match
    if (password !== password2) {
        errors.push({ msg: 'Passwords do not match' })
    }

    // Check password length
    if (password.length < 8) {
        errors.push({ msg: 'Password should be at least 8 characters' })
    }

    // Check if all the characters are present in string
    function validate(string) {
        // Initialize counter to zero
        var counter = 0;

        // On each test that is passed, increment the counter
        if (/[a-z]/.test(string)) {
            // If string contain at least one lowercase alphabet character
            counter++;
        }
        if (/[A-Z]/.test(string)) {
            counter++;
        }
        if (/[0-9]/.test(string)) {
            counter++;
        }
        if (/[!@#$&*]/.test(string)) {
            counter++;
        }

        // Check if at least three rules are satisfied
        return counter >= 3;
    }

    var passChars = validate(password);

    if (passChars == false) {
        errors.push({ msg: 'Password should contain at least 3 of the following: a) lower case letter b) upper case letter c) number d) special character' })
    }

    // is the email whitelisted?
    if (email) {
        var enteredEmail = email;
        var exists = whitelist.filter(item => {
            return item.email === enteredEmail;
        });

        // if not then user may not register
        if (exists.length < 1) {
            return res.send("Please get in touch to register");
        }

        // check verification code
        if (exists && code) {
            console.log("exists and code: ", exists, code)
            var enteredCode = code;
            var matches = exists.filter(item => {
                return item.code === enteredCode;
            });
            console.log("matches: ", matches)

            if (matches.length < 1) {
                errors.push({ msg: 'Incorrect verification code' })
            }
        }
    }

    if (errors.length > 0) {
        // re-render the registration form, and display error messages and the values (pass them to EJS)
        res.render('register', {
            errors,
            name,
            lname,
            email,
            code,
            password,
            password2
        })
    } else {
        // Validation Passed, so we create a user
        // first ensure user doesnt exist
        User.findOne({ email: email })
            .then(user => {
                if (user) {
                    // User exists
                    errors.push({ msg: 'Email is already registered' })
                    res.render('register', {
                        errors,
                        name,
                        lname,
                        email,
                        code,
                        password,
                        password2
                    })
                } else {
                    // check if users email is on approved admin list
                    var adminMatch = admins.filter(x => {
                        return x.email === email;
                    });


                    var isAdmin = adminMatch.length < 1 ? false : true;


                    const newUser = new User({
                        _id: new mongoose.Types.ObjectId(),
                        name,
                        lname,
                        email,
                        password,
                        isAdmin
                    });

                    // Hash Password
                    bcrypt.genSalt(10, (err, salt) =>
                        bcrypt.hash(newUser.password, salt, (err, hash) => {
                            if (err) throw err;
                            // set password to hash
                            newUser.password = hash

                            // Save user
                            newUser.save()
                                .then(user => {
                                    // this creates a flash message, but doesnt display it.
                                    // to display the msg, you need to use ejs
                                    req.flash('success_msg', 'You are now registered and can log in')
                                    res.redirect('/users/login')
                                })
                                .catch(err => console.log(err))
                        }))

                }
            })
    }
})

// Login handle
router.post('/login', (req, res, next) => {
    passport.authenticate('local', {
        successRedirect: '/dashboard',
        failureRedirect: '/users/login',
        failureFlash: true
    })(req, res, next);
})

// Logout handle
router.get('/logout', (req, res) => {
    req.logout();
    req.flash('success_msg', 'You are logged out');
    res.redirect('/users/login');
})

router.get('/forgot', (req, res) => {
    res.render("forgot")
});

router.post('/forgot', function (req, res, next) {
    async.waterfall([
        function (done) {
            crypto.randomBytes(20, function (err, buf) {
                var token = buf.toString('hex');
                done(err, token);
            });
        },
        function (token, done) {
            User.findOne({ email: req.body.email }, function (err, user) {
                if (!user) {
                    req.flash('error', 'No account with that email address exists.');
                    return res.redirect('/users/forgot');
                }

                user.resetPasswordToken = token;
                user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

                user.save(function (err) {
                    done(err, token, user);
                });
            });
        },
        function (token, user, done) {
            var smtpTransport = nodemailer.createTransport({
                host: database.host,
                port: 587,
                secure: false, // true for 465, false for other ports
                auth: {
                    user: database.email,
                    pass: database.password
                },
                logger: true
            });
            var mailOptions = {
                from: `"title" <${database.email}>`, // sender address
                to: user.email, // list of receivers
                subject: "Password reset request", // Subject line
                text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
                    'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
                    'http://' + req.headers.host + '/users/reset/' + token + '\n\n' +
                    'If you did not request this, please ignore this email and your password will remain unchanged.\n'
            };
            smtpTransport.sendMail(mailOptions, function (err) {
                console.log('mail sent');
                req.flash('success_msg', 'An e-mail has been sent to ' + user.email + ' with further instructions. Please allow up to 10 minutes for the email to arrive');
                done(err, 'done');
            });
        }
    ], function (err) {
        if (err) return next(err);
        res.redirect('/users/abc/sent');
    });
});

// get the reset password page
router.get('/reset/:token', function (req, res) {
    User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function (err, user) {
        if (!user) {
            req.flash('error_msg', 'Password reset token is invalid or has expired.');
            return res.redirect('/users/forgot');
        }
        res.render('reset', { token: req.params.token });
    });
});

// Password Reset
router.post('/reset/:token', function (req, res) {
    User.findOne({
        resetPasswordToken: req.params.token,
        resetPasswordExpires: { $gt: Date.now() }
    }).then(user => {
        // if no user, redirect to forgot password page
        if (!user) {
            req.flash("error_msg", "Password token invalid or has expired");
            return res.redirect("/users/forgot");
        } else {
            const { password, password2 } = req.body;
            let errors = [];
            // //first do password validation
            // // 1 check required fields
            if (!password || !password2) {
                errors.push({ msg: "Please fill in all fields" });
            }
            // 2 check passwords match
            if (password !== password2) {
                errors.push({ msg: "Passwords do not match" });
            }
            // Check password length
            if (password.length < 8) {
                errors.push({ msg: 'Password should be at least 8 characters' })
            }

            // Check if all the characters are present in string
            function validate(string) {
                // Initialize counter to zero
                var counter = 0;

                // On each test that is passed, increment the counter
                if (/[a-z]/.test(string)) {
                    // If string contain at least one lowercase alphabet character
                    counter++;
                }
                if (/[A-Z]/.test(string)) {
                    counter++;
                }
                if (/[0-9]/.test(string)) {
                    counter++;
                }
                if (/[!@#$&*]/.test(string)) {
                    counter++;
                }

                // Check if at least three rules are satisfied
                return counter >= 3;
            }

            var passChars = validate(password);

            if (passChars == false) {
                errors.push({ msg: 'Password should contain at least 3 of the following: a) lower case letter b) upper case letter c) number d) special character' })
            }


            if (errors.length > 0) {
                //TODO render these errors instead of flash
                errors.forEach(error => {
                    req.flash("error_msg", error.msg);
                });
                return res.redirect("/users/reset/" + req.params.token);
            } else {
                // Hash Password
                bcrypt.genSalt(10, (err, salt) =>
                    bcrypt.hash(password, salt, (err, hash) => {
                        if (err) throw err;
                        //set password to hashed
                        //save user
                        user.set({ password: hash });
                        user.set({ resetPasswordToken: undefined });
                        user.set({ resetPasswordExpires: undefined });
                        user.save()
                            .then(user => {
                                // we have to use the flash msg b/c we're redirecting so we're storing it in the session
                                req.flash(
                                    "success_msg",
                                    "You have successfully reset your password"
                                );
                                res.redirect("/users/login");
                            })
                            .catch(err => console.log(err));
                    })
                );
            }
        }
    });
});

module.exports = router;