## Choir Website

This is a website used by our choir for storage and communication purposes. 

* It's where we store all our sheet music and audio files. There is a search filter to find files quicker by typing the song name
* We can be notified of important information
* We can respond to upcoming performances (re our availability to perform)
* Admins are able to post/edit/delete events and notifications

![home page](/public/images/home-page.png)

The screenshots below are taken from an admins viewpoint. The first screenshot shows the dashboard and the second, the performances page where users can confirm or decline participation

![dashboard](/public/images/dashboard2.png)

![performances page](/public/images/events2.png)

## Installation

```
git clone <path>
cd <directory>
npm install
```

### Setup

### Mongo
Structure for the database file in config folder:

```
const mongoose = require('mongoose');

const MONGO_USERNAME = 'username';
const MONGO_PASSWORD = 'password';
const MONGO_HOSTNAME = 'hostname';
const MONGO_PORT = 'port';
const MONGO_DB = 'db';

const uri = `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}?authSource=admin`;

module.exports = {
    uri
}
```

### Nodemailer
Set up config for Nodemailer
```
const config = {
    databaseStr: "database string",
    host: "host",
    email: "email",
    password: "password",
    personal: "personal email"
};

module.exports = config;
```

### Whitelist
#### Members
Only members of the choir are able to use this website, so there needs to be whitelisted email addresses list. 
Members need a unique code to register, for a bit of security. The code should be a string of approx 6 random chars

#### Admins
The email addresses of members who are intended to be admins should also be specified

```
const whitelist = [
    {
        email: "email",
        code: "random string of chars"
    },
    {
        email: "email",
        code: "random string of chars"
    },
     {
        email: "email",
        code: "random string of chars"
    },
     {
        email: "email",
        code: "random string of chars"
    },
]

const admins = [
    { email: "email" },
    { email: 'email' }
]

module.exports = { whitelist, admins };

```

### Session
A file containing the session key
```
module.exports = { "session": "key" } 
```
