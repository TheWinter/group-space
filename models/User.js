
let mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new mongoose.Schema({
    _id: Schema.Types.ObjectId,
    name: {
        type: String,
        required: true
    },
    lname: {
        type: String,
        // required: true TODO uncomment this line
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    isAdmin: {
        type: Boolean
    },
    isUpload: {
        type: Boolean
    },
    resetPasswordToken: String,
    resetPasswordExpires: Date,
    date: {
        type: Date,
        default: Date.now
    },
    last_active: {
        type: Date,
        default: Date.now
    }
});

// updates the last active property
UserSchema.statics.login = function login(id, callback) {
    return this.findByIdAndUpdate(id, { $set: { 'last_active': Date.now() } }, callback);
};

const User = mongoose.model("User", UserSchema);

module.exports = { User, UserSchema };