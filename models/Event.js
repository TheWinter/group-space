let mongoose = require('mongoose');
const Schema = mongoose.Schema;
const User = require('./User.js').User;

const EventSchema = new mongoose.Schema({
    _id: Schema.Types.ObjectId,
    title: String,
    description: String,
    date: { type: Date, default: Date.now },
    nastup: { type: Date, required: true },
    // Array of subdocuments
    // this is how you create a many to many
    participants: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    declines: [{ type: Schema.Types.ObjectId, ref: 'User' }],
    cancellations: [{ type: Schema.Types.ObjectId, ref: 'User' }],

});

const Event = mongoose.model("Event", EventSchema);


module.exports = Event;